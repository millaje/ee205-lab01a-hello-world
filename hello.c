/////////////////////////////////////////////////////////////////////////////// // University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01 - Hello World
//
// @author Jeraldine Milla <millaje@hawaii.edu>
// @date   11 Jan 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int main() {
	printf("Hello world!\n");
	printf("I am Sam\n");
}

